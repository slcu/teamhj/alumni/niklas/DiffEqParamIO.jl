module DiffEqParamIO

using DiffEqBase
using FileIO
using JLD2

ODEType = Union{DiffEqBase.AbstractReactionNetwork, DiffEqBase.AbstractParameterizedFunction}

export append_param!, remove_param, get_params

include("param_io.jl")

end
