# DiffEqParamIO.jl

Supply basic file IO for parameters belonging to some [DifferentialEquations](https://github.com/JuliaDiffEq/DifferentialEquations.jl) based model.

This package currently only supports dumb Array parameter sets, not the potentially better (yet unfinished) [DiffEqParameters](https://gitlab.com/slcu/teamHJ/niklas/DiffEqParameters.jl) types.
